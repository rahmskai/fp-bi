<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ route('dashboard') }}"><i class='fa fa-line-chart'></i> <span>Dashboard</span></a></li>
            <li><a href="{{ route('bts.list') }}"><i class='fa fa-table'></i> <span>Daftar BTS</span></a></li>
            <li><a href="{{ url('/logout') }}"><i class='fa fa-sign-out'></i> <span>Logout</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
