<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Target
 */
class Target extends Model
{
    protected $table = 'target';

    public $timestamps = false;

    protected $fillable = [
        'tBulan',
        'tTahun',
        'tTarget',
        'status'
    ];

    protected $guarded = [];

        
}