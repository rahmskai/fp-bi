<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Paket
 */
class Paket extends Model
{
    protected $table = 'paket';

    public $timestamps = false;

    protected $fillable = [
        'paketName',
        'quota'
    ];

    protected $guarded = [];

        
}