<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bts
 */
class Bts extends Model
{
    protected $table = 'bts';

    public $timestamps = false;

    protected $fillable = [
        'city',
        'tipe'
    ];

    protected $guarded = [];

    public function type()
    {
    	return $this->belongsTo('App\Models\BtsType', 'tipe');
    }
}